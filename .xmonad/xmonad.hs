import XMonad				-- all the essentials
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import Data.Monoid
import System.Exit
import XMonad.Util.Ungrab
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Config.Desktop		-- defaultConfig
import XMonad.Hooks.ManageDocks		-- docks, avoidStruts
import XMonad.Layout.ThreeColumns	-- ThreeColMid
import XMonad.Layout.Magnifier		-- magnifiercz'
import XMonad.Layout.Spacing
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
-- import XMonad.Util.ClickableWorkspaces -- use UnsafeMonadLog for things like this


main :: IO ()
main	= xmonad
	. docks
	. ewmh
	=<< statusBar "xmobar" myXmobarPP toggleStrutsKey myConfig
	where
		toggleStrutsKey :: XConfig Layout -> (KeyMask, KeySym)
		toggleStrutsKey XConfig{ modMask = m } = (m, xK_b)

-- DEFINITIONS --

myTerminal		= "alacritty"
myModMask		= mod4Mask
myBorderWidth		= 2	

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"
myWorkspaces		= ["web", "code2", "code3", "code4", "misc5", "misc6", "misc7", "misc8", "msg"]

myLayout = spacingRaw False (Border 0 10 10 10) True (Border 10 10 10 10) True $
	avoidStruts $ tiled ||| Mirror tiled ||| Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

myXmobarPP :: PP
myXmobarPP = def
	{ ppSep			= bluishgray "|"
	, ppTitleSanitize	= xmobarStrip
	, ppCurrent		= lightblue . wrap (white "[") (white "]")
	, ppHidden		= white . wrap "" ""
	, ppHiddenNoWindows	= wrap "" ""
	, ppUrgent		= red . wrap (yellow "!") (yellow "!")
	, ppOrder		= \[ws, l, _] -> [ws]
	}
	where
		formatFocused	= wrap (white "[") (white "]") . lightblue . ppWindow
		fomatUnfocused	= wrap (lowWhite "[") (lowWhite "]") . lightblue 
		ppWindow :: String -> String
		ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30
		
		blue, white, lowWhite, magenta, red, yellow :: String -> String
		magenta		= xmobarColor "#ff79c6" ""
		blue		= xmobarColor "#bd93f9" ""
		white		= xmobarColor "white" ""
		yellow		= xmobarColor "#f1fa8c" ""
		red			= xmobarColor "#ff5555" ""
		lowWhite	= xmobarColor "#a9a9a9" ""
		lightblue	= xmobarColor "#68C8F8" ""
		bluishgray	= xmobarColor "#618CA1" ""

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
	--, title 	=? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 0 )
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

-- myEventHook	= return ()
-- myLogHook	= return () 

-- Startup Hook - Runs when Xmonad starts --
myStartupHook	= do
	spawn "setxkbmap -option caps:escape"

-- Key and Mouse bindings --
myKeys conf@(XConfig {XMonad.modMask = modm}) = keys def conf `M.union` M.fromList
	[
		-- launch a terminal
		((modm .|. shiftMask, xK_Return), spawn $ terminal conf)

		-- launch dmenu
		, ((modm,               xK_p     ), spawn "dmenu_run")

		-- close focused window
		, ((modm .|. shiftMask, xK_c     ), kill)

		 -- Rotate through the available layout algorithms
		, ((modm,               xK_space ), sendMessage NextLayout)

		--  Reset the layouts on the current workspace to default
		, ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

		-- Resize viewed windows to the correct size
		, ((modm,               xK_n     ), refresh)

		-- Move focus to the next window
		, ((modm,               xK_Tab   ), windows W.focusDown)

		-- Move focus to the next window
		, ((modm,               xK_j     ), windows W.focusDown)

		-- Move focus to the previous window
		, ((modm,               xK_k     ), windows W.focusUp  )

		-- Move focus to the master window
		, ((modm,               xK_m     ), windows W.focusMaster  )

		-- Swap the focused window and the master window
		, ((modm,               xK_Return), windows W.swapMaster)

		-- Swap the focused window with the next window
		, ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

		-- Swap the focused window with the previous window
		, ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

		-- Shrink the master area
		, ((modm,               xK_h     ), sendMessage Shrink)

		-- Expand the master area
		, ((modm,               xK_l     ), sendMessage Expand)

		-- Push window back into tiling
		, ((modm,               xK_t     ), withFocused $ windows . W.sink)

		-- Increment the number of windows in the master area
		, ((modm              , xK_comma ), sendMessage (IncMasterN 1))

		-- Deincrement the number of windows in the master area
		, ((modm              , xK_period), sendMessage (IncMasterN (-1)))

		-- Toggle the status bar gap
		-- Use this binding with avoidStruts from Hooks.ManageDocks.
		-- See also the statusBar function from Hooks.DynamicLog.
		--
		-- , ((modm              , xK_b     ), sendMessage ToggleStruts)

		-- Quit xmonad
		, ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

		-- Restart xmonad
		, ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

		-- Run xmessage with a summary of the default keybindings (useful for beginners)
		, ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
	
		-- Toggle spaces
		, ((modm .|. shiftMask, xK_s     ), do toggleScreenSpacingEnabled ; toggleWindowSpacingEnabled)

		-- Dmenu search prompt
		, ((modm              , xK_s     ), spawn "~/.local/bin/dmenu_websearch")

		-- Dmenu exec prompt
		-- , ((modm              , xK_e     ), spawn "~/.local/bin/dmenu_exec")

		-- Dmenu wifi prompt
		, ((modm .|. shiftMask, xK_t     ), spawn "~/.local/bin/dmenu_wifi")

		-- Dmenu screenshot prompt
		, ((0		          , xK_Print ), spawn "~/.local/bin/dmenu_scrot")

		-- Open file manager
		, ((modm              , xK_d     ), spawn "pcmanfm")

		-- Lock X
		, ((modm .|. shiftMask, xK_d	 ), spawn "physlock")
	
		-- toggle play/pause
		, ((modm .|. controlMask, xK_m   ), safeSpawn "mpc" ["toggle"])
	
		-- open ncmpcpp
		, ((modm .|. shiftMask, xK_m     ), spawn  "alacritty -e ncmpcpp")

	]

-- Mouse bindings --
myMouseBindings conf@(XConfig {XMonad.modMask = modm}) = mouseBindings def conf `M.union` M.fromList
    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


myConfig = def 
	{
		terminal		= myTerminal,
		focusFollowsMouse	= myFocusFollowsMouse,
		borderWidth		= myBorderWidth,
		modMask			= myModMask,
		workspaces		= myWorkspaces,
		normalBorderColor	= myNormalBorderColor,
		focusedBorderColor	= myFocusedBorderColor,
		
		-- key bindings
		keys               = myKeys,
		mouseBindings      = myMouseBindings,
			
		-- hooks, layouts
		layoutHook         = myLayout,
		manageHook         = myManageHook,
		-- handleEventHook    = myEventHook,
		-- logHook            = myLogHook,
		startupHook        = myStartupHook
	}	



-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
